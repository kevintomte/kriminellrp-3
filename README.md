# KriminellRP utvecklings kontor #
Välkommna mina kära undersåtar! Här hittar du källkoden för alla viktiga filer och även en "Issue tracker" för ytterligare fixes/requests!

### Snabb länkar ###
* [Tutorial för admins](http://soon.com/)
* [Hur man använder konsolet](http://soon.com/)

### To do / problem att fixa ###
En fullständig lista finns här: https://bitbucket.org/KriminellRP/kriminellrp/issues

### Andra fixes ###
Listen med grejor som inte finns ovan.
* 16-07-27: Fixade "Failed to load 32-bit libtinfo.so.5 or libncurses.so.5."
* 15-11-21: Översatte klientfiler och överförde de till /addons/ - "ds_xx.gma" då en del inte laddades ner via WorkshopDL.