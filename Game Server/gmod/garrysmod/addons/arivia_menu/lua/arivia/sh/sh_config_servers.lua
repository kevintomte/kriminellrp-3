Arivia.ServersEnabled = true
Arivia.ServerButtonColor = Color(15, 15, 15, 0)
Arivia.ServerButtonHoverColor = Color(255, 255, 255, 220)
Arivia.ServerButtonTextColor = Color(255, 255, 255, 255)
Arivia.ServerButtonHoverTextColor = Color(0, 0, 0, 255)
Arivia.UseServerIconsWithText = true
Arivia.UseServerIconsOnly = false
Arivia.Servers = {
    {
        hostname = "VÄLKOMMEN TILL KRMINELLRP!",
        icon = "arivia/arivia_btn_server.png",
        ip = ""
    }
}