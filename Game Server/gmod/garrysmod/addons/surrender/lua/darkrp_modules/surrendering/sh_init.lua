DarkRP.registerDarkRPVar("In_Surrender_state",net.WriteBit,fc{tobool,net.ReadBit})
DarkRP.registerDarkRPVar("Is_Cable_tied",net.WriteBit,fc{tobool,net.ReadBit})

local plyMeta = FindMetaTable("Player")

function plyMeta:IsSurrendering()
	return (self:getDarkRPVar("In_Surrender_state") or false)
end

--There's nothing related to cables, except through the code... Just didnt want to change it... Whole reason why players look "tied" and why this is called "cable"
--was due to me being inspired by PAYDAY 2
function plyMeta:IsCableTied()
	return (self:getDarkRPVar("Is_Cable_tied") or false)
end