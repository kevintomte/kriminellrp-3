local plyMeta = FindMetaTable("Player")

local pelvis = "ValveBiped.Bip01_Pelvis"
local r_calf = "ValveBiped.Bip01_R_Calf"
local l_calf = "ValveBiped.Bip01_L_Calf"
local r_thigh = "ValveBiped.Bip01_R_Thigh"
local l_thigh = "ValveBiped.Bip01_L_Thigh"
local r_foot = "ValveBiped.Bip01_R_Foot"
local l_foot = "ValveBiped.Bip01_L_Foot"
local r_upperarm = "ValveBiped.Bip01_R_UpperArm"
local l_upperarm = "ValveBiped.Bip01_L_UpperArm"
local r_forearm = "ValveBiped.Bip01_R_ForeArm"
local l_forearm = "ValveBiped.Bip01_L_ForeArm"
local r_hand = "ValveBiped.Bip01_R_Hand"
local l_hand = "ValveBiped.Bip01_L_Hand"
local head = "ValveBiped.Bip01_Head1"
local function reset_surrender_bones(ply)
	if not ply:GetBoneCount() then return end
	for i=0, ply:GetBoneCount() do
		ply:ManipulateBonePosition(i,Vector(0,0,0))
		ply:ManipulateBoneAngles(i,Angle(0,0,0))
	end
end

local origin = Vector(0,0,0)
local cheapbonemanips = {}
cheapbonemanips["surrender"] = {
	[pelvis] = {Vector(0,0,-23),Angle(0,0,0)},
	[r_calf] = {origin,Angle(0,120,0)},
	[l_calf] = {origin,Angle(0,120,0)},
	[r_thigh] = {origin,Angle(0,-30,0)},
	[l_thigh] = {origin,Angle(0,-30,0)},
	[r_foot] = {origin,Angle(0,30,0)},
	[l_foot] = {origin,Angle(0,30,0)},
	[r_upperarm] = {origin,Angle(30,0,90)},
	[l_upperarm] = {origin,Angle(-30,0,-90)},
	[r_forearm] = {origin,Angle(0,-130,0)},
	[l_forearm] = {origin,Angle(0,-120,20)}
}

cheapbonemanips["tied"] = {
	[pelvis] = {Vector(0,0,-35),Angle(0,0,90)},
	[r_calf] = {origin,Angle(0,-12,0)},
	[l_calf] = {origin,Angle(0,-19,0)},
	[l_thigh] = {origin,Angle(0,10,0)},
	[r_foot] = {origin,Angle(0,60,0)},
	[l_foot] = {origin,Angle(0,60,0)},
	[r_upperarm] = {origin,Angle(-15,30,0)},
	[l_upperarm] = {origin,Angle(15,20,0)},
	[r_forearm] = {origin,Angle(-30,-30,40)},
	[l_forearm] = {origin,Angle(30,-30,-40)},
	[head] = {origin,Angle(0,45,75)},
	[r_hand] = {origin,Angle(0,0,-120)},
	[l_hand] = {origin,Angle(0,0,90)}
}

function plyMeta:DoSurrender(surrtime)
	if self:IsSurrendering() or self:IsCableTied() or not self:Alive() then return end
	if SurrenderSettings["cansurrender"](self) == false then return end
	self:ConCommand("-duck")
	self:setDarkRPVar("In_Surrender_state",true)
	local org_walk_speed = self:GetWalkSpeed()
	local org_run_speed = self:GetRunSpeed()
	timer.Simple(0,function()
		if not IsValid(self) then return end
		self:SetWalkSpeed(1)
		self:SetRunSpeed(1)
	end)
	self:Give("weapon_surrender")
	self:SelectWeapon("weapon_surrender")
	
	for k,v in pairs(cheapbonemanips["surrender"]) do
		local boneid = self:LookupBone(k)
		if boneid then
			self:ManipulateBonePosition(boneid,v[1])
			self:ManipulateBoneAngles(boneid,v[2])
		end
	end
	
	local entindex = self:EntIndex()
	hook.Add("Think","surrendering_YOULIKEEXPLOITSHM?"..entindex,function() if IsValid(self) then self:SelectWeapon("weapon_surrender") else hook.Remove("Think","surrendering_YOULIKEEXPLOITSHM?"..entindex) end end)
	timer.Create("surrender_stop_surrendering"..entindex,surrtime,1,function()
		if IsValid(self) then
			reset_surrender_bones(self)
			self:StripWeapon("weapon_surrender")
			self:setDarkRPVar("In_Surrender_state",false)
			self:SetWalkSpeed(org_walk_speed)
			self:SetRunSpeed(org_run_speed)
			hook.Remove("Think","surrendering_YOULIKEEXPLOITSHM?"..entindex)
		end
	end)
end

function plyMeta:TiePlayer()
	if self:IsCableTied() then return end
	timer.Destroy("surrender_stop_surrendering"..self:EntIndex())
	hook.Remove("Think","surrendering_YOULIKEEXPLOITSHM?"..self:EntIndex())
	self:setDarkRPVar("In_Surrender_state",false)
	reset_surrender_bones(self)
	
	self:setDarkRPVar("Is_Cable_tied",true)
	
	self:StripWeapon("weapon_surrender")
	self:Give("weapon_cable_tied")
	self:SelectWeapon("weapon_cable_tied")
	
	for k,v in pairs(cheapbonemanips["tied"]) do
		local boneid = self:LookupBone(k)
		if boneid then
			self:ManipulateBonePosition(boneid,v[1])
			self:ManipulateBoneAngles(boneid,v[2])
		end
	end
	
	self:Lock()
	self:EmitSound("physics/cardboard/cardboard_box_strain"..math.random(1,3)..".wav",70,math.random(90,110))

	local entindex = self:EntIndex()
	hook.Add("Think","surrendering_YOULIKEEXPLOITSHM?2"..entindex,function() if IsValid(self) then self:SelectWeapon("weapon_cable_tied") else hook.Remove("Think","surrendering_YOULIKEEXPLOITSHM?2"..entindex) end end)
	timer.Create("surrender_cable_ties_auto_untie"..self:EntIndex(),600,1,function()
		if not IsValid(self) then return end
		self:setDarkRPVar("Is_Cable_tied",false)
		reset_surrender_bones(self)

		self:StripWeapon("weapon_cable_tied")
	
		self:UnLock()
		self:EmitSound("physics/wood/wood_strain2.wav",70,math.random(90,110))
	
		self:SetWalkSpeed(GAMEMODE.Config.walkspeed)
		self:SetRunSpeed(GAMEMODE.Config.runspeed)
		
		hook.Remove("Think","surrendering_YOULIKEEXPLOITSHM?2"..entindex)
	end)
	
	timer.Simple(0,function()
		if not IsValid(self) then return end
		self:SetWalkSpeed(1)
		self:SetRunSpeed(1)
	end)
end

hook.Add("ScalePlayerDamage","surrendering_less_damage",function(ply,hitgroup,dmginfo)
	if ply:IsSurrendering() then
		dmginfo:ScaleDamage(SurrenderSettings["surrenderdamagescale"])
	end
end)

hook.Add("CanPlayerSuicide","surrendering_check_suicide",function(ply)
	if ply:IsSurrendering() or ply:IsCableTied() then
		ply:ChatPrint("You cant suicide whilst in a surrender/tied state!")
		return false
	end
end)
--^^^^ Some people like to kill themself to get away

hook.Add("PlayerDeath","surrendering_destroy_timer_on_death",function(ply)
	if ply:IsSurrendering() or ply:IsCableTied() then
		timer.Destroy("surrender_stop_surrendering"..ply:EntIndex())
		ply:setDarkRPVar("In_Surrender_state",false)
		ply:setDarkRPVar("Is_Cable_tied",false)
		reset_surrender_bones(ply)

		timer.Destroy("surrender_cable_ties_auto_untie"..ply:EntIndex())
	end
end)

hook.Add("playerCanChangeTeam","surrendering_STOP_SWITCHING_JOBS",function(ply)
	if ply:IsSurrendering() or ply:IsCableTied() then DarkRP.notify(ply,1,5,"You cannot switch job whilst surrendering/tied") return false end --Disallow even if forced, it could cause issues otherwise
end)
hook.Add("canArrest","surrendering_prevent_arrest",function(ply,arr)
	if arr:IsSurrendering() then return false, "Surrendering players cannot be arrested" end
end)
hook.Add("playerArrested","surrendering_untie_on_arrest",function(ply1,tiem,ply2)
	timer.Destroy("surrender_cable_ties_auto_untie"..ply1:EntIndex())
	ply1:setDarkRPVar("Is_Cable_tied",false)
	reset_surrender_bones(ply1)

	ply1:StripWeapon("weapon_cable_tied")
	
	ply1:UnLock()
end)
hook.Add("canUnarrest","surrendering_prevent_unarrest",function(ply,arr)
	if arr:IsSurrendering() or arr:IsCableTied() then return false, "Surrendering/tied players cannot be unarrested" end
end)

hook.Add("KeyPress","surrendering_open_interact_menu",function(ply,key)
	if key == IN_USE then
		if ply:IsCableTied() or ply:IsSurrendering() then return end
		local trace = ply:GetEyeTrace()
		if trace.Entity:IsValid() and trace.Entity:IsPlayer() then
			if trace.HitPos:Distance(ply:GetPos()) < 100 then
				if trace.Entity:IsSurrendering() or trace.Entity:IsCableTied() then
					SendUserMessage("surrender_open_interact_menu",ply,trace.Entity)
				end
			end
		end
	end
end)

hook.Add("KeyPress","surrendering_do_surrender",function(ply,key)
	if ply:KeyDown(IN_DUCK) and ply:KeyDown(IN_SPEED) and key == IN_USE then
		if SurrenderSettings["cansurrender"](ply) == false then return end
		ply:ConCommand("-duck")
		SendUserMessage("surrender_confirm_dialog",ply)
	end
end)

local function dothelogicstrip(class)
	if not class then return false end
	local plsno = SurrenderSettings["weapontostrip"]
	plsno["weapon_surrender"] = SurrenderSettings["weapontostrip_invert"]
	plsno["weapon_cable_tied"] = SurrenderSettings["weapontostrip_invert"]
	local checkwep = plsno[class]
	if SurrenderSettings["weapontostrip_invert"] then
		if not checkwep then
			return true
		end
	else
		if checkwep then
			return true
		end
	end
end
concommand.Add("surrender_strip_weapons",function(ply,cmd,args)
	if not args[1] then return end
	if not isnumber(tonumber(args[1])) then return end
	if ply:IsCableTied() or ply:IsSurrendering() then return end
	local pl = Entity(tonumber(args[1]))
	if not IsValid(pl) then return end
	if pl == ply then return end
	if ply:GetPos():Distance(pl:GetPos()) > 100 then return end
	if pl:IsSurrendering() or pl:IsCableTied() then
	for k,v in pairs(pl:GetWeapons()) do
		if dothelogicstrip(v:GetClass()) then
			if ply:HasWeapon(v:GetClass()) then
				local ent = ents.Create("spawned_weapon")
				ent:SetModel(v:GetModel())
				ent:SetPos(ply:GetShootPos())
				ent:SetModel(v:GetModel())
				ent:Spawn()
				ent:SetWeaponClass(v:GetClass())
				ent:SetCollisionGroup(COLLISION_GROUP_DISSOLVING)
			else
				ply:Give(v:GetClass())
			end
			v:Remove()
		end
	end
	pl:SendLua("chat.AddText(Color(255,65,65),'Your weapons have been taken!')")
	end
end)

concommand.Add("surrender_cable_tie",function(ply,cmd,args)
	if not args[1] then return end
	if not isnumber(tonumber(args[1])) then return end
	if ply:IsCableTied() or ply:IsSurrendering() then return end
	local pl = Entity(tonumber(args[1]))
	if not IsValid(pl) then return end
	if pl == ply then return end
	if ply:GetPos():Distance(pl:GetPos()) > 100 then return end
	if not pl:IsSurrendering() then return end
	local cantie, msg = SurrenderSettings["cantie"](ply,pl)
	if cantie == false then
		ply:ChatPrint(msg or "You cant tie this person down. I dont know why, so dont ask")
		return
	end
	pl:TiePlayer()
end)

concommand.Add("surrender_uncable_tie",function(ply,cmd,args)
	if not args[1] then return end
	if not isnumber(tonumber(args[1])) then return end
	if ply:IsCableTied() or ply:IsSurrendering() then return end
	local pl = Entity(tonumber(args[1]))
	if not IsValid(pl) then return end
	if pl == ply then return end
	if ply:GetPos():Distance(pl:GetPos()) > 100 then return end
	if not pl:IsCableTied() then return end
	timer.Destroy("surrender_cable_ties_auto_untie"..pl:EntIndex())
	pl:setDarkRPVar("Is_Cable_tied",false)
	reset_surrender_bones(pl)

	pl:StripWeapon("weapon_cable_tied")
	
	pl:UnLock()
	pl:EmitSound("physics/wood/wood_strain2.wav",70,math.random(90,110))
	pl:SendLua("chat.AddText(Color(65,150,65),'Someone broke off your restraints!')")
	
	hook.Remove("Think","surrendering_YOULIKEEXPLOITSHM?2"..pl:EntIndex())
	
	pl:SetWalkSpeed(GAMEMODE.Config.walkspeed)
	pl:SetRunSpeed(GAMEMODE.Config.runspeed)
end)

concommand.Add("surrender_surrender",function(ply,cmd,args)
	if ply:GetVelocity():Length() > 5 then return end
	if ply:Crouching() then return end
	if not ply:OnGround() then return end
	if SurrenderSettings["cansurrender"](ply) == false then return end
	ply:DoSurrender(15)
end)

local function dothelogic(ply,reversed)
	if not IsValid(ply:GetActiveWeapon()) then return false end
	local class = ply:GetActiveWeapon():GetClass()
	local plsno = SurrenderSettings["intimidatingweapons"]
	plsno["weapon_surrender"] = SurrenderSettings["intimidatingweapons_invert"]
	plsno["weapon_cable_tied"] = SurrenderSettings["intimidatingweapons_invert"]
	local checkwep = plsno[ply:GetActiveWeapon():GetClass()]
	if SurrenderSettings["intimidatingweapons_invert"] then
		if reversed then
			if checkwep then
				return true
			end
		else
			if not checkwep then
				return true
			end
		end
	else
		if reversed then
			if not checkwep then
				return true
			end
		else
			if checkwep then
				return true
			end
		end
	end
end
concommand.Add("surrender_shout",function(ply)
	if not ply:Alive() then return end
	if dothelogic(ply,false) then
		if not ply.LastShoutDelayer then ply.LastShoutDelayer = CurTime() end
		if ply.LastShoutDelayer > CurTime() then return end
		--Trace shit from weapon_fists.lua packed with Gmod
		local trace = util.TraceLine( {
			start = ply:GetShootPos(),
			endpos = ply:GetShootPos() + ply:GetAimVector() * 250,
			filter = ply
		} )

		if ( !IsValid( trace.Entity ) ) then 
			trace = util.TraceHull( {
				start = ply:GetShootPos(),
				endpos = ply:GetShootPos() + ply:GetAimVector() * 250,
				filter = ply,
				mins = Vector( -10, -10, -8 ),
				maxs = Vector( 10, 10, 8 )
			} )
		end
		
		timer.Simple(0.1,function()
			if trace.Entity:IsValid() and trace.Entity:IsPlayer() then
				if SurrenderSettings["cansurrender"](trace.Entity) == false then return end
				if SurrenderSettings["canshout"](ply,trace.Entity) == false then return end
				if dothelogic(trace.Entity,true) then
					if trace.Entity:GetVelocity():Length() > 60 then
						DarkRP.talkToRange(ply,ply:GetName(),table.Random(SurrenderSettings["forcesurrender_messages_running"]),250)
						BroadcastLua("Entity("..ply:EntIndex().."):AnimRestartGesture(0,55,true)")
						SendUserMessage("surrender_shout_viewmodel",ply)
					elseif trace.Entity:IsSurrendering() or trace.Entity:IsCableTied() then
						DarkRP.talkToRange(ply,ply:GetName(),table.Random(SurrenderSettings["forcesurrender_messages_surrendering"]),250)
						BroadcastLua("Entity("..ply:EntIndex().."):AnimRestartGesture(0,ACT_GMOD_GESTURE_MELEE_SHOVE_2HAND,true)")
						SendUserMessage("surrender_shout_viewmodel",ply)
					else
						DarkRP.talkToRange(ply,ply:GetName(),table.Random(SurrenderSettings["forcesurrender_messages_standing"]),250)
						BroadcastLua("Entity("..ply:EntIndex().."):AnimRestartGesture(0,ACT_HL2MP_GESTURE_RANGE_ATTACK_MELEE2,true)")
						SendUserMessage("surrender_shout_viewmodel",ply)
					end
					ply.LastShoutDelayer = CurTime() + 1.5
					if math.random(1,SurrenderSettings["forcesurrender_chance"]) == 1 then
						trace.Entity:DoSurrender(SurrenderSettings["forcesurrender_time"])
					end
				end
			end
		end)
	end
end)