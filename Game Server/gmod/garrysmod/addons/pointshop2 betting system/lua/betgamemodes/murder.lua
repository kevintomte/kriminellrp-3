--Pointshop betting gamemode scripts are SHARED. Hooks must return the same data on both server and client.
--This script is for the Murder gamemode by MechanicalMind

--Gamemode scripts MUST ALWAYS specify the first and second teams to bet on with these settings:
BETTING.FirstTeam = {2,"Bystanders", Color(26, 120, 245), function(ply) if SERVER then return !ply:GetMurderer() else return !gmod.GetGamemode():GetAmMurderer() end end}
BETTING.SecondTeam = {1,"The Murderer", Color(255, 77, 77), function(ply) if SERVER then return ply:GetMurderer() else return gmod.GetGamemode():GetAmMurderer() end end}
//You could use this file to load custom settings based on the gamemode e.g.
//BETTING.Settings.MinimumPlayersForBetting = 5

local function PlayerCanBetMurder(ply)
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime enabled
	if tonumber(BETTING.Settings.OnlyAllowBettingAtRoundStartTime) and BETTING.Settings.OnlyAllowBettingAtRoundStartTime > 0 then
		if BETTING.EndBetsTime and CurTime() <= BETTING.EndBetsTime then return true end
		return false,string.format("You must place a bet in the first %i seconds of the round!",BETTING.Settings.OnlyAllowBettingAtRoundStartTime)
	end
	
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime disabled
	if ply:Alive() then return false,"Players must be dead or spectating to bet." end
	if gmod.GetGamemode():GetRound() != 1 then return false,"You cant bet whilst a new round is preparing." end
	return true
end
hook.Add("PlayerCanBet","PlayerCanBetMurder",PlayerCanBetMurder)

local function AllowBetsWhenRoundBegins()
	if SERVER then SendUserMessage("PSBettingOnStartRound", player.GetAll()) end
	BETTING.EndBetsTime = CurTime() + BETTING.Settings.OnlyAllowBettingAtRoundStartTime or 30
end
if CLIENT then usermessage.Hook("PSBettingOnStartRound",AllowBetsWhenRoundBegins) end
hook.Add("OnStartRound","AllowBetsWhenRoundBegins",AllowBetsWhenRoundBegins)

--SERVER hooks
--Every outcome must call BETTING.FinishBets else the bet window will not be closed
if SERVER then
	local OldEndTheRound = gmod.GetGamemode().EndTheRound
	local function MurderEndRoundBets(gm,reason,murderer)
		OldEndTheRound(gm,reason,murderer)
		if not reason then return end
		if (reason == 3) then BETTING.FinishBets(reason, true) //Cancel all bets on rage quit
		else BETTING.FinishBets(reason) end
	end
	gmod.GetGamemode().EndTheRound = MurderEndRoundBets
end