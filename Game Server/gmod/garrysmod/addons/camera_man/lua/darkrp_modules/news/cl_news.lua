--[[
	Copyright (C) Chessnut - All Rights Reserved
	Unauthorized copying of this file, via any medium is strictly prohibited. Proprietary and confidential.
	Written by Chessnut (chessnutist@gmail.com), December 2014
--]]

surface.CreateFont("newsNumber", {
	font = "Arial Black",
	size = 54,
	weight = 1000
})

surface.CreateFont("newsTiny", {
	font = "Arial",
	size = 14,
	weight = 200
})

surface.CreateFont("newsItalics", {
	font = "Arial",
	size = 28,
	weight = 800,
	italic = true
})

surface.CreateFont("newsNormal", {
	font = "Arial",
	size = 28,
	weight = 800
})

net.Receive("NewsTicker", function(length)
	local client = net.ReadEntity()
	local text = net.ReadString()

	NewsTickers[client] = text:upper()
end)

net.Receive("NewsTickerFull", function(length)
	NewsTickers = net.ReadTable()
end)