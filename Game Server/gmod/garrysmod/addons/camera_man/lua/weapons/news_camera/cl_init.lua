--[[
	Copyright (C) Chessnut - All Rights Reserved
	Unauthorized copying of this file, via any medium is strictly prohibited. Proprietary and confidential.
	Written by Chessnut (chessnutist@gmail.com), December 2014
--]]

include("shared.lua")

SWEP.Category = "News Mod"
SWEP.PrintName = "News Camera"
SWEP.Author = "Chessnut"
SWEP.Purpose = "Used to broadcast a scene."
SWEP.Slot = 5
SWEP.DrawAmmo = false

function SWEP:GetViewModelPosition(position, angles)
	position = position + angles:Right()*15 + angles:Forward()*18

	return position, angles
end

function SWEP:PrimaryAttack()
	local menu = DermaMenu()
		local sounds, panel = menu:AddSubMenu("Play Sound")
		panel:SetToolTip("Play a sound to everyone tuned in to your news channel.")
		panel:SetImage("icon16/sound.png")

		for k, v in ipairs(NEWS_SOUNDS) do
			sounds:AddOption(v[1], function()
				RunConsoleCommand("news_sound", k)
			end):SetImage(v[3])
		end

		local ticker = menu:AddOption("Edit Ticker", function()
			Derma_StringRequest("Ticker Text", "Edit what is shown at the bottom of the channel.", NewsTickers[LocalPlayer()] or "Sample Text", function(text)
				net.Start("NewsTicker")
					net.WriteString(text)
				net.SendToServer()
			end)
		end)
		ticker:SetImage("icon16/textfield_rename.png")
		ticker:SetToolTip("Edit what is shown at the bottom of the channel.")
	menu:Open()
	menu:Center()
end

function SWEP:SecondaryAttack()
end

function SWEP:DrawWorldModel()
	if (!self.dummy) then
		self.dummy = ClientsideModel(self.ViewModel)
	end

	self.dummy:SetNoDraw(false)
		local info = self:GetAttachment(1)
		local position, angles

		if (info) then
			position, angles = info.Pos, info.Ang
		else
			position, angles = self.Owner:GetShootPos(), self.Owner:EyeAngles()
		end

		self.dummy:SetPos(position - angles:Forward()*10 + angles:Up()*6 + angles:Right()*3)
		self.dummy:SetAngles(angles)
		self.dummy:DrawModel()
	self.dummy:SetNoDraw(true)
end