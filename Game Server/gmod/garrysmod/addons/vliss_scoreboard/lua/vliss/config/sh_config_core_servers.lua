-----------------------------------------------------------------
-- @package     Vliss
-- @authors     Richard
-- @build       v1.4.0
-- @release     12.29.2015
-----------------------------------------------------------------

-----------------------------------------------------------------
--  Server Listings
-----------------------------------------------------------------

Vliss.ServersEnabled = true
Vliss.ServerButtonColor = Color(15, 15, 15, 0)
Vliss.ServerButtonHoverColor = Color(255, 255, 255, 220)
Vliss.ServerButtonTextColor = Color(255, 255, 255, 255)
Vliss.ServerButtonHoverTextColor = Color(0, 0, 0, 255)
Vliss.UseServerIconsWithText = true 
Vliss.UseServerIconsOnly = false  

Vliss.Servers = {
    {
        hostname = "EXAMPLE SERVER",
        icon = "vliss/vliss_btn_server.png",
        ip = "127.0.0.1:27015"
    },
    {
        hostname = "EXAMPLE SERVER 2",
        icon = "vliss/vliss_btn_server.png",
        ip = "127.0.0.1:27015"
    }
}