{"ExtraSeats":{"1":{"Pos":"[16.26 4.23 30]","DoorSounds":true,"Ang":"{0 0 0}","EnterRange":80,"RadioControl":true},"2":{"Pos":"[-16.26 -35 30]","DoorSounds":true,"Ang":"{0 0 0}","EnterRange":80,"RadioControl":true},"3":{"Pos":"[16.26 -35 30]","DoorSounds":true,"Ang":"{0 0 0}","EnterRange":80,"RadioControl":true}},"Date":"08/01/14 17:21:57","Lights":{"1":{"UseSprite":true,"Pos":"[-33.24 -95.35 33.04]","UseDynamic":true,"UseNormal":true,"Sprite":{"Size":0.6},"Dynamic":{"Brightness":2,"Size":0.6},"NormalColor":{"1":255,"2":55,"3":0}},"2":{"UseSprite":true,"UseBrake":true,"UseDynamic":true,"Sprite":{"Size":0.6},"BrakeColor":{"1":255,"2":55,"3":0},"Pos":"[-33.53 -95.64 38.46]","Dynamic":{"Brightness":2,"Size":0.6}},"3":{"UseBlinkers":true,"Pos":"[-34.46 -95.63 42.56]","UseDynamic":true,"BlinkersColor":{"1":255,"2":155,"3":0},"UseSprite":true,"Dynamic":{"Brightness":2,"Size":0.4},"Sprite":{"Size":0.4}},"4":{"Sprite":{"Size":0.15},"Dynamic":{"Brightness":2,"Size":0.35},"UseReverse":true,"UseSprite":true,"ReverseColor":{"1":200,"2":225,"3":255},"SpecLine":{"Amount":5,"Pos":"[-15.89 -96.68 42.61]","Use":true},"Pos":"[-15.89 -96.68 36.5]","UseDynamic":true},"5":{"UseBlinkers":true,"Pos":"[-34.77 94.92 25.28]","UseDynamic":true,"BlinkersColor":{"1":255,"2":155,"3":0},"UseSprite":true,"Dynamic":{"Brightness":2,"Size":0.4},"Sprite":{"Size":0.4}},"6":{"Sprite":{"Size":1.2},"Dynamic":{"Brightness":2,"Size":0.6},"UseHead":true,"UseSprite":true,"Pos":"[-30.68 90.62 34.37]","UseDynamic":true,"HeadColor":{"1":196,"2":206,"3":255},"UsePrjTex":true,"ProjTexture":{"Forward":30,"Size":2024,"Angle":"{0 90 0}"}},"7":{"UseSprite":true,"Pos":"[-23.42 89.82 34.5]","UseDynamic":true,"UseNormal":true,"Sprite":{"Size":0.6},"Dynamic":{"Brightness":2,"Size":0.6},"NormalColor":{"1":233.67,"2":200.29,"3":227.57}},"8":{"UseSprite":true,"Pos":"[-26.66 91.61 18.14]","UseDynamic":true,"UseNormal":true,"Sprite":{"Size":0.6},"Dynamic":{"Brightness":2,"Size":0.6},"NormalColor":{"1":233.67,"2":200.29,"3":227.57}},"9":{"UseSprite":true,"Pos":"[33.24 -95.35 33.04]","UseDynamic":true,"UseNormal":true,"Sprite":{"Size":0.6},"Dynamic":{"Brightness":2,"Size":0.6},"NormalColor":{"1":255,"2":55,"3":0}},"10":{"UseSprite":true,"UseBrake":true,"UseDynamic":true,"Sprite":{"Size":0.6},"BrakeColor":{"1":255,"2":55,"3":0},"Pos":"[33.53 -95.64 38.46]","Dynamic":{"Brightness":2,"Size":0.6}},"11":{"UseBlinkers":true,"Pos":"[34.46 -95.63 42.56]","UseDynamic":true,"BlinkersColor":{"1":255,"2":155,"3":0},"UseSprite":true,"Dynamic":{"Brightness":2,"Size":0.4},"Sprite":{"Size":0.4}},"12":{"Sprite":{"Size":0.15},"Dynamic":{"Brightness":2,"Size":0.35},"UseReverse":true,"UseSprite":true,"ReverseColor":{"1":200,"2":225,"3":255},"SpecLine":{"Amount":5,"Pos":"[15.89 -96.68 42.61]","Use":true},"Pos":"[15.89 -96.68 36.5]","UseDynamic":true},"13":{"UseBlinkers":true,"Pos":"[34.77 94.92 25.28]","UseDynamic":true,"BlinkersColor":{"1":255,"2":155,"3":0},"UseSprite":true,"Dynamic":{"Brightness":2,"Size":0.4},"Sprite":{"Size":0.4}},"14":{"Sprite":{"Size":1.2},"Dynamic":{"Brightness":2,"Size":0.6},"UseHead":true,"UseSprite":true,"Pos":"[30.68 90.62 34.37]","UseDynamic":true,"HeadColor":{"1":196,"2":206,"3":255},"UsePrjTex":true,"ProjTexture":{"Forward":30,"Size":2024,"Angle":"{0 90 0}"}},"15":{"UseSprite":true,"Pos":"[23.42 89.82 34.5]","UseDynamic":true,"UseNormal":true,"Sprite":{"Size":0.6},"Dynamic":{"Brightness":2,"Size":0.6},"NormalColor":{"1":233.67,"2":200.29,"3":227.57}},"16":{"UseSprite":true,"Pos":"[26.66 91.61 18.14]","UseDynamic":true,"UseNormal":true,"Sprite":{"Size":0.6},"Dynamic":{"Brightness":2,"Size":0.6},"NormalColor":{"1":233.67,"2":200.29,"3":227.57}}},"Exhaust":{"1":{"Ang":"{0 -90 0}","Pos":"[-31.04 -93.58 17.48]","EffectStress":"VC_Exhaust_Stress","EffectIdle":"VC_Exhaust"}},"Author":"freemmaann vcmod.club"}