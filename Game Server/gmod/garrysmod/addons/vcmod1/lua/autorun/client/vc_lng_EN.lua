// Copyright © 2012-2015 VCMod (freemmaann). All Rights Reserved. if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.
local Lng = {}

//You do not have to translate everything. Things that are not translated will be written in English.

//Set up the language
Lng.Name = "English"
Lng.Language_Code = "EN"
Lng.Translated_By_Name = "freemmaann"
Lng.Translated_By_Link = "http://steamcommunity.com//id/freemmaann/"
Lng.Translated_Date = "2015 03 14"

////////////// Translation starts here

////////GENERAL
Lng.Locked = "Vehicle locked."
Lng.UnLocked = "Vehicle unlocked."
Lng.Chat = 'To adjust options (car lights, view) type "!vcmod" in chat.'
Lng.Broken = "This vehicle is broken, it needs to be repaired."
Lng.Trl_Atch = "Trailer attached."
Lng.Trl_Detch = "Trailer detached."
Lng.ELS_TuningIntoPoliceC = "Tuning into police chatter"
Lng.ELS_NoPoliceRCFound = "No police chatter found."

////////Pickup
Lng.TouchCar100 = "Touch a car to repair it to 100%."
Lng.TouchCar25 = "Touch a car with to repair it with +25% of its original health."
Lng.TouchCar10 = "Touch a car with to repair it with +10% of its original health."

////////MENU general
Lng.Info = "Info"
Lng.Menu = "Menu"
Lng.Language = "Language"
Lng.Personal = "Personal"
Lng.Administrator = "Administrator"
Lng.Options = "Options"
Lng.ELSOptions = "ELS Options"
Lng.Main = "Main"
Lng.Controls = "Controls"
Lng.HUD = "HUD"
Lng.View = "View"
Lng.Radio = "Radio"
Lng.Multiplier = "Multiplier"
Lng.OptOnly_You = "These options will only affect you"
Lng.OptOnly_Admin = "These settings can only be changed by an administrator"
Lng.NPC_Settings = "NPC settings"
Lng.Height = "Height"
Lng.FadeOutDistance = "Fade out distance"
Lng.Reset = "Reset"
Lng.Save = "Save"
Lng.Load = "Load"
Lng.Volume = "Volume"
Lng.Distance = "Distance"
Lng.None = "None"
Lng.EnterKey = "Enter key"

Lng.Enabled_Cl = "Enabled clientside"
Lng.Enabled_Sv = "Enabled serverside"

Lng.Lights = "Lights"
Lng.Health = "Health"
Lng.Sound = "Sound"
Lng.Other = "Other"
Lng.CreatedBy = "Created by"
Lng.Enabled = "Enabled"
Lng.OffTime = "Off time"
Lng.Time = "Time"
Lng.DistMultiplier = "Distance multiplier"
Lng.ControlsReset = "Controls reset."
Lng.SettingsSaved = "Settings saved."
Lng.SettingsReset = "Settings reset."
Lng.LoadedSettingsFromServer = "Settings loaded from server."
Lng.InteriorIndicators = "Interior indicators"
Lng.ExtraGlow = "Extra glow"

////////MENU ELS
Lng.ManulSiren = "Manual siren"
Lng.ELS_SirenSwitch = "ELS siren switch"
Lng.ELS_SirenToggle = "ELS siren toggle"
Lng.ELS_LightsSwitch = "ELS lights switch"
Lng.ELS_LightsToggle = "ELS lights toggle"
Lng.VCModMainEnabled = "VCMod main enabled"
Lng.VCModELSEnabled = "VCMod ELS enabled"
Lng.ELSLightsEnabled = "ELS lights enabled"
Lng.AutoDisableELSLights = "Auto disable ELS lights"
Lng.OffOnExit = "Off on exit"
Lng.Siren = "Siren"
Lng.AutoDisableELSSounds = "Auto disable ELS sounds"
Lng.Manual = "Manual"
Lng.Bullhorn = "Bullhorn"
Lng.PoliceChatterEnabled = "Police chatter enabled"
Lng.PoliceChatter = "Police chatter"
Lng.PoliceChatter_Info = "Police chatter is a real time feed of official police chatter VIA radio."
Lng.SelectedRadioChatter = "Selected radio chatter channel"
Lng.ReduceDamageToEmergencyVehicles = "Reduce damage to emergency vehicles"

////////MENU personal info
Lng.YouAreUsingVCMod = "You are using VCMod"
Lng.ServerIsUsingVCMod = "This server is using VCMod"
Lng.Info_EverThought = "Ever thought that gmod's vehicles are not realistic or missing things from them?\nVCMod is designed to make garrysmod vehicles as good as in any other game."
Lng.Info_VCModHasFollowingAddons = "VCMod has the following addons:"

////////MENU personal options
Lng.VisDist = "Visibility distance"
Lng.Warmth = "Warmth"
Lng.Lines = "Lines"
Lng.Glow = "Glow"
Lng.DynamicLights = "Dynamic lights"
Lng.ThirdPView = "Third person view"
Lng.DynamicView = "Dynamic view"
Lng.AutoFocus = "Auto focus"
Lng.Reverse = "Reverse"
Lng.VectorStiffness = "Vector stiffness %"
Lng.AngleStiffness = "Angle stiffness %"
Lng.IgnoreWorld = "Ignore world"
Lng.TruckView = "Truck connected view"

////////MENU controls
Lng.HoldDuration = "Hold duration"
Lng.Mouse = "Mouse"
Lng.KeyboardInput = "Keyboard input"
Lng.MouseInput = "Mouse input"

Lng.NightLights = "Night lights"
Lng.HeadLights = "Head lights"
Lng.LowHigh = "Low/High beam toggle"
Lng.HazardLights = "Hazard lights"
Lng.BlinkerLeft = "Blinker left"
Lng.BlinkerRight = "Blinker right"
Lng.Horn = "Horn"
Lng.Cruise = "Cruise"
Lng.LockUnlock = "Lock/Unlock"
Lng.LookBehind = "Look behind"
Lng.DetachTrl = "Detach trailer"

////////MENU hud
Lng.Effect3D = "3D effect"
Lng.HUDHeight = "Side HUD height %"
Lng.HUD_Name = "Name"
Lng.HUD_Icons = "Icons"
Lng.HUD_Cruise = "Cruise"
Lng.HUD_Cruise_MPH = "mi/h instead of km/h"
Lng.HUD_Repair = "Repair"
Lng.HUD_ELS_Siren = "ELS siren"
Lng.HUD_ELS_Lights = "ELS lights"

////////MENU admin options
Lng.HandbrakeLights = "Handbrake lights"
Lng.InteriorLights = "Interior lights"
Lng.BlinkersOffExit = "Blinkers (turn signals) off on exit"

Lng.DamageEnabled = "Damage enabled"
Lng.StartHealthMultiplier = "Start health multiplier"
Lng.PhysicalDamage = "Physical damage"
Lng.FireDuration = "Fire duration"
Lng.RemoveCarAfterExplosion = "Remove car after explosion"
Lng.ReducePlayerDmgWhileInCar = "Reduce player damage while in a car"

Lng.DoorSounds = "Door sounds"
Lng.TruckRevBeep = "Truck reverse sound"

Lng.SteeringWheelLOnExit = "Steering wheel lock on exit"
Lng.BrakesLOnExit = "Brakes lock on exit"
Lng.WheelDust = "Wheel dust"
Lng.WheelDustWhileBraking = "Wheel dust while braking"
Lng.MatchPlayerSpeedExit = "Match player speed on exit"
Lng.NoCollidePlyOnExit = "No collide players with car on exit"
Lng.Exhaust = "Exhaust"
Lng.PassengerSeats = "Passenger seats"
Lng.TrailerAttach = "Trailer attach"
Lng.TrailerAttachConStrengthM = "Trailer attach connection strength multiplier"
Lng.TrailersCanAtchToReg = "Trailer can attach to regular cars"
Lng.RepairToolSpeedMult = "Repair tool speed multiplier"

if !VC_Lng_T then VC_Lng_T = {} end VC_Lng_T[Lng.Language_Code] = Lng